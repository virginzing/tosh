class RenameMedia < ActiveRecord::Migration[5.2]
  def change
    rename_table :medium_categories, :media_categories
  end
end
