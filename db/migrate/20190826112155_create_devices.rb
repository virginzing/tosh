class CreateDevices < ActiveRecord::Migration[5.2]
  def change
    create_table :devices do |t|
      t.references :member, foreign_key: true
      t.string :token

      t.timestamps
    end
  end
end
