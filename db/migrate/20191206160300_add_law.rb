class AddLaw < ActiveRecord::Migration[5.2]
  def change
    Medium.transaction do
	    category = MediaCategory.find_or_create_by(name: 'law')

      Medium.where(category: category.name).destroy_all
	    laws = File.readlines(Rails.root.join('app', 'media', 'law', '0.txt'))

	    laws.each_with_index.map do |law, law_index|
	      folders = File.readlines(Rails.root.join('app', 'media', 'law', "#{law_index + 1}", '0.txt'))

	      folders.each_with_index.map do |folder, folder_index|
	        filenames = File.readlines(Rails.root.join('app', 'media', 'law', "#{law_index + 1}", "#{folder_index + 1}.txt"))

	        filenames.each_with_index.map do |filename, file_index|
	          url = "/file/pdf/app/media/law/#{law_index + 1}/#{folder_index + 1}/#{file_index + 1}.pdf"
	          sequence = file_index + 1
	          year = filename.split('พ.ศ. ').last.gsub(/\s/, '')
year = year.size == 4 ? year : nil
	          Medium.create!(
	            title: filename,
	            category: category.name,
	            sequence: sequence,
	            url: url,
	            meta: { year: year, category: law.split(' ').first, sub_category: folder.split(' ').first }
	          )
	        end
    		end
    	end
    end
  end
end
