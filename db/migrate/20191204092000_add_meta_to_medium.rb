class AddMetaToMedium < ActiveRecord::Migration[5.2]
  def change
    add_column :media, :meta, :json
  end
end
