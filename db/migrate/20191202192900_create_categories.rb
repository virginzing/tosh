class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :medium_categories do |t|
      t.string :name

      t.timestamps
    end
    
    create_table :question_categories do |t|
      t.string :name

      t.timestamps
    end

    %w[ebook infographic standard video].map { |name| MediumCategory.create(name: name) }
    
    %w[conflagration sme].map { |name| QuestionCategory.create(name: name) }
  end
end
