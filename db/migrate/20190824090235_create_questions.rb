class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|
      t.string :title
      t.string :category
      t.boolean :is_sample
      t.integer :sequence
      t.jsonb :data

      t.timestamps
    end
  end
end
