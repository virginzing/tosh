class AddSequenceToMedium < ActiveRecord::Migration[5.2]
  def change
    add_column :media, :sequence, :integer
  end
end
