class CreateTitleQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :title_questions do |t|
      t.string :title

      t.timestamps
    end

    add_reference :title_questions, :parent, index: true
    add_reference :questions, :parent, index: true
  end
end
