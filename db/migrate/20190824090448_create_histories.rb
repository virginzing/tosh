class CreateHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :histories do |t|
      t.references :member, foreign_key: true
      t.jsonb :data

      t.timestamps
    end
  end
end
