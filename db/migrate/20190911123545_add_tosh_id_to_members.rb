class AddToshIdToMembers < ActiveRecord::Migration[5.2]
  def change
    add_column :members, :tosh_id, :string
  end
end
