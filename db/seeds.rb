## drop all
models = [TitleQuestion, Question, Member, History, Device, Medium]
models.map do |model|
  ActiveRecord::Base.connection.execute("TRUNCATE #{model.table_name} CASCADE")
end

## sample questions sme
Question.transaction do
title = TitleQuestion.create!(title: '1. นโยบายด้านความปลอดภัยฯ')
  Question.create!(
    title: '1.1 นายจ้างต้องกำหนดนโยบายความปลอดภัยฯ โดยจัดทำเป็นเอกสารพร้อมทั้งลงนาม',
    category: 'sme',
    is_sample: true,
    sequence: 1,
    parent: title
  )
  Question.create!(
    title: '1.2 นายจ้างต้องให้ลูกจ้างมีส่วนร่วมในการกำหนดนโยบายด้านความปลอดภัยฯ',
    category: 'sme',
    is_sample: true,
    sequence: 2,
    parent: title
  )
  Question.create!(
    title: '1.3 มีการทบทวนนโยบายด้านความปลอดภัยฯ ตามระยะเวลาที่เหมาะสม เพื่อให้มั่นใจว่านโยบายที่กำหนดขึ้นมีความเหมาะสมกับสถานประกอบกิจการ',
    category: 'sme',
    is_sample: true,
    sequence: 3,
    parent: title
  )
  Question.create!(
    title: '1.4 มีการสื่อสารให้ลูกจ้างทราบอย่างทั่วถึง',
    category: 'sme',
    is_sample: true,
    sequence: 4,
    parent: title
  )
title = TitleQuestion.create!(title: '2. โครงสร้างการบริหารด้านความปลอดภัยฯ')
  Question.create!(
    title: '2.1 นายจ้างต้องกำหนดโครงสร้างการบริหารด้านความปลอดภัยฯ พร้อมทั้งกำหนดบทบาท อำนาจหน้าที่และความรับผิดชอบของหน่วยงาน คณะบุคคล และบุคลากรด้านความปลอดภัยฯ เป็นเอกสารและมีการสื่อสารให้ลูกจ้างทราบอย่างทั่วถึง',
    category: 'sme',
    is_sample: true,
    sequence: 5,
    parent: title
  )
title = TitleQuestion.create!(title: '3. แผนงานด้านความปลอดภัยฯ และการนำไปปฏิบัติ')
  sub_title = TitleQuestion.create!(
    title: '3.1 การชี้บ่งอันตรายและการประเมินความเสี่ยง',
    parent: title
  )
    Question.create!(
      title: '(1) นายจ้างต้องจัดทำขั้นตอนการดำเนินงานการชี้บ่งอันตรายและการประเมินระดับความเสี่ยง',
      category: 'sme',
      is_sample: true,
      sequence: 6,
      parent: sub_title
    )
    sub2_title = TitleQuestion.create!(
      title: '(2) นายจ้างต้องชี้บ่งอันตรายและประเมินระดับความเสี่ยง',
      parent: sub_title
    )
      Question.create!(
        title: 'ก. กิจกรรมที่ทำเป็นประจำและไม่เป็นประจำ',
        category: 'sme',
        is_sample: true,
        sequence: 7,
        parent: sub2_title
      )
      Question.create!(
        title: 'ข. พฤติกรรมที่ไม่ปลอดภัยของลูกจ้าง ขีดความสามารถของร่างกาย และท่าทางในการทำงานที่ไม่เหมาะสม',
        category: 'sme',
        is_sample: true,
        sequence: 8,
        parent: sub2_title
      )
      Question.create!(
        title: 'ค. กิจกรรมและสภาพแวดล้อมในการทำงานของผู้รับเหมา บุคคลภายนอก และผู้เยี่ยมชม',
        category: 'sme',
        is_sample: true,
        sequence: 9,
        parent: sub2_title
      )
      Question.create!(
        title: 'ง. การเปลี่ยนแปลงหรือข้อเสนอให้มีการเปลี่ยนแปลงวัสดุอุปกรณ์ กระบวนการ วิธีปฏิบัติงาน หรือกิจกรรมต่าง ๆ ในสถานประกอบกิจการ',
        category: 'sme',
        is_sample: true,
        sequence: 10,
        parent: sub2_title
      )
      Question.create!(
        title: 'จ. การปรับปรุงระบบการจัดการด้านความปลอดภัยฯ รวมถึงการเปลี่ยนแปลงชั่วคราวและมีผลต่อการปฏิบัติงาน กระบวนการ และกิจกรรมต่าง ๆ',
        category: 'sme',
        is_sample: true,
        sequence: 11,
        parent: sub2_title
      )
      Question.create!(
        title: 'ฉ. เมื่อเกิดอุบัติเหตุหรือเหตุการณ์เกือบเกิดอุบัติเหตุ และมีการสอบสวนแล้ว',
        category: 'sme',
        is_sample: true,
        sequence: 12,
        parent: sub2_title
      )
title = TitleQuestion.create!(title: '3. แผนงานด้านความปลอดภัยฯ และการนำไปปฏิบัติ')
  sub_title = TitleQuestion.create!(
    title: '3.1 การชี้บ่งอันตรายและการประเมินความเสี่ยง',
    parent: title
  )
    Question.create!(
      title: '(3) นายจ้างต้องทบทวนการชี้บ่งอันตรายและประเมินระดับความเสี่ยงตามช่วงเวลาที่กำหนด',
      category: 'sme',
      is_sample: true,
      sequence: 13,
      parent: sub_title
    )
    Question.create!(
      title: '(4) นายจ้างต้องให้ผู้รับเหมา มีส่วนร่วมในการชี้บ่งอันตรายและประเมินระดับความเสี่ยง พร้อมทั้งมีการสื่อสารความเสี่ยงไปยังลูกจ้าง ผู้มีส่วนได้เสีย และผู้ที่เกี่ยวข้อง',
      category: 'sme',
      is_sample: true,
      sequence: 14,
      parent: sub_title
    )
    Question.create!(
      title: '(5) นายจ้างต้องจัดทำและเก็บบันทึกการชี้บ่งอันตรายและการประเมินความเสี่ยง',
      category: 'sme',
      is_sample: true,
      sequence: 15,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '3.2 กฎหมายความปลอดภัยฯ',
    parent: title
  )
    Question.create!(
      title: '(1) นายจ้างต้องจัดทำขั้นตอนการดำเนินงาน ในการชี้บ่งและติดตามกฎหมายความปลอดภัยฯ ที่เกี่ยวข้องกับสถานประกอบกิจการ ให้เป็นปัจจุบันอยู่เสมอ',
      category: 'sme',
      is_sample: true,
      sequence: 16,
      parent: sub_title
    )
    Question.create!(
      title: '(2) นายจ้างต้องปฏิบัติตามข้อกำหนดของกฎหมาย และกำหนดผู้รับผิดชอบในการนำกฎหมายไปปฏิบัติและรักษาไว้ในสถานประกอบกิจการ',
      category: 'sme',
      is_sample: true,
      sequence: 17,
      parent: sub_title
    )
    Question.create!(
      title: '(3) นายจ้างมีการประเมินความสอดคล้องในการปฏิบัติตามกฎหมายด้านความปลอดภัยฯ',
      category: 'sme',
      is_sample: true,
      sequence: 18,
      parent: sub_title
    )
    Question.create!(
      title: '(4) นายจ้างต้องจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
      category: 'sme',
      is_sample: true,
      sequence: 19,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '3.3 วัตถุประสงค์และแผนงาน',
    parent: title
  )
    Question.create!(
      title: '(1) นายจ้างต้องจัดทำวัตถุประสงค์ด้านความปลอดภัยฯ โดยจัดทำเป็นเอกสาร',
      category: 'sme',
      is_sample: true,
      sequence: 20,
      parent: sub_title
    )
    sub2_title = TitleQuestion.create!(
      title: '(2) นายจ้างต้องจัดทำแผนงานเป็นเอกสารเพื่อให้บรรลุวัตถุประสงค์ของสถานประกอบกิจการ',
      parent: sub_title
    )
      Question.create!(
        title: 'ก. การกำหนดความรับผิดชอบและอำนาจหน้าที่ในระดับและหน่วยงานที่เกี่ยวข้อง ภายในสถานประกอบกิจการ',
        category: 'sme',
        is_sample: true,
        sequence: 21,
        parent: sub2_title
      )
      Question.create!(
        title: 'ข. กรอบเวลาดำเนินการ',
        category: 'sme',
        is_sample: true,
        sequence: 22,
        parent: sub2_title
      )
title = TitleQuestion.create!(title: '3. แผนงานด้านความปลอดภัยฯ และการนำไปปฏิบัติ')
  sub_title = TitleQuestion.create!(
    title: '3.3 วัตถุประสงค์และแผนงาน',
    parent: title
  )
    Question.create!(
      title: '(3) ดำเนินการตรวจติดตามและวัดผลการปฏิบัติงานอย่างสม่ำเสมอเพื่อให้บรรลุตามวัตถุประสงค์และแผนงาน พร้อมทั้งปรับเปลี่ยนตามความเหมาะสม',
      category: 'sme',
      is_sample: true,
      sequence: 23,
      parent: sub_title
    )
    Question.create!(
      title: '(4) นายจ้างต้องจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
      category: 'sme',
      is_sample: true,
      sequence: 24,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '3.4 สมรรถนะและการฝึกอบรม',
    parent: title
  )
    Question.create!(
      title: '(1) นายจ้างต้องชี้บ่งความจำเป็นในการฝึกอบรมที่เกี่ยวข้องกับความเสี่ยง และระบบการจัดการด้านความปลอดภัยฯ โดยจัดให้มีการฝึกอบรมให้สอดคล้องกับความจำเป็นในการฝึกอบรมที่กำหนดไว้ ประเมินประสิทธิผลของการฝึกอบรม และทบทวนเป็นระยะ',
      category: 'sme',
      is_sample: true,
      sequence: 25,
      parent: sub_title
    )
    Question.create!(
      title: '(2) นายจ้างต้องจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
      category: 'sme',
      is_sample: true,
      sequence: 26,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '3.5 การสื่อสาร',
    parent: title
  )
    Question.create!(
      title: '(1) นายจ้างต้องจัดทำขั้นตอนการดำเนินงานในการสื่อสารที่เกี่ยวข้องกับอันตราย และระบบการจัดการด้านความปลอดภัยฯ ทั้งภายในและภายนอกสถานประกอบกิจการ ที่เกี่ยวข้องกับผู้รับเหมา บุคคลภายนอก และผู้เยี่ยมชมในสถานที่ทำงาน',
      category: 'sme',
      is_sample: true,
      sequence: 27,
      parent: sub_title
    )
    Question.create!(
      title: '(2) นายจ้างต้องจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
      category: 'sme',
      is_sample: true,
      sequence: 28,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '3.6 การจัดทำเอกสาร
นายจ้างจะต้องจัดทำขั้นตอนการดำเนินงานในการควบคุมเอกสาร ในระบบการจัดการด้านความปลอดภัยฯ เพื่อให้เอกสารมีความทันสมัย และสามารถใช้ตามวัตถุประสงค์ที่ต้องการ โดยต้องควบคุมดังนี้',
    parent: title
  )
    Question.create!(
      title: '(1) มีการจัดเก็บบันทึกทางด้านความปลอดภัยฯ ตามระยะเวลาที่กำหนดไว้',
      category: 'sme',
      is_sample: true,
      sequence: 29,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '3.7 การควบคุมการปฏิบัติงาน
นายจ้างต้องกำหนดขั้นตอนเพื่อจัดการความเสี่ยงจากการปฏิบัติงาน และกิจกรรมต่าง ๆ ซึ่งได้มีการกำหนดไว้ว่าจะต้องมีการดำเนินการ โดยขั้นตอนเพื่อจัดการความเสี่ยง ต้องประกอบด้วย',
    parent: title
  )
    Question.create!(
      title: '(1) ขั้นตอนการดำเนินงาน วิธีการปฏิบัติงาน สำหรับกิจกรรมที่มีความเสี่ยง',
      category: 'sme',
      is_sample: true,
      sequence: 30,
      parent: sub_title
    )
    Question.create!(
      title: '(2) การปฏิบัติตามเกณฑ์การควบคุมการปฏิบัติงานที่ได้กำหนดไว้',
      category: 'sme',
      is_sample: true,
      sequence: 31,
      parent: sub_title
    )
    Question.create!(
      title: '(3) การเตือนอันตราย',
      category: 'sme',
      is_sample: true,
      sequence: 32,
      parent: sub_title
    )
    Question.create!(
      title: '(4) นายจ้างต้องจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
      category: 'sme',
      is_sample: true,
      sequence: 33,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '3.8 การเตรียมความพร้อมและการตอบโต้ภาวะฉุกเฉิน',
    parent: title
  )
    Question.create!(
      title: '(1) นายจ้างมีการชี้บ่งภาวะฉุกเฉินที่อาจจะเกิดขึ้น',
      category: 'sme',
      is_sample: true,
      sequence: 34,
      parent: sub_title
    )
    Question.create!(
      title: '(2) ในการวางแผนตอบโต้ภาวะฉุกเฉิน นายจ้างต้องพิจารณาถึงการประสานงานกับหน่วยงานที่เกี่ยวข้อง ทั้งในการขอความช่วยเหลือและการแจ้งเหตุ',
      category: 'sme',
      is_sample: true,
      sequence: 35,
      parent: sub_title
    )
    Question.create!(
      title: '(3) ทำการฝึกอบรมให้กับลูกจ้างทุกคนในสถานประกอบกิจการ รวมทั้งทำการฝึกซ้อมแผนตอบโต้ภาวะฉุกเฉินตามช่วงเวลาที่กำหนดไว้',
      category: 'sme',
      is_sample: true,
      sequence: 36,
      parent: sub_title
    )
    Question.create!(
      title: '(4) นายจ้างต้องมีการตรวจสอบอุปกรณ์ที่ใช้ในภาวะฉุกเฉินเป็นระยะ',
      category: 'sme',
      is_sample: true,
      sequence: 37,
      parent: sub_title
    )
    Question.create!(
      title: '(5) นายจ้างต้องจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
      category: 'sme',
      is_sample: true,
      sequence: 38,
      parent: sub_title
    )
title = TitleQuestion.create!(title: '4. การประเมินผลและการทบทวนการจัดการด้านความปลอดภัยฯ')
  sub_title = TitleQuestion.create!(
    title: '4.1 การประเมินผล',
    parent: title
  )
    sub2_title = TitleQuestion.create!(
      title: '(1) การเฝ้าระวังและการวัดผลการปฏิบัติงาน
นายจ้างต้องจัดทำขั้นตอนการดำเนินงานในการเฝ้าระวังและวัดผลการปฏิบัติงาน อย่างสม่ำเสมอ โดยขั้นตอนการดำเนินงานจะต้องครอบคลุมถึง',
      parent: sub_title
    )
      Question.create!(
        title: 'ก. การตรวจวัดสภาพแวดล้อมในการทำงาน',
        category: 'sme',
        is_sample: true,
        sequence: 39,
        parent: sub2_title
      )
      Question.create!(
        title: 'ข. การตรวจสุขภาพของลูกจ้างตามปัจจัยเสี่ยง',
        category: 'sme',
        is_sample: true,
        sequence: 40,
        parent: sub2_title
      )
      Question.create!(
        title: 'ค. การตรวจสอบเครื่องจักรอุปกรณ์',
        category: 'sme',
        is_sample: true,
        sequence: 41,
        parent: sub2_title
      )
      Question.create!(
        title: 'ง. การตรวจสอบอุปกรณ์คุ้มครองความปลอดภัยส่วนบุคคล',
        category: 'sme',
        is_sample: true,
        sequence: 42,
        parent: sub2_title
      )
    sub2_title = TitleQuestion.create!(
      title: '(2) การสอบสวนอุบัติการณ์
นายจ้างต้องจัดทำขั้นตอนการดำเนินงานสำหรับสอบสวนและวิเคราะห์อุบัติการณ์ โดย',
      parent: sub_title
    )
      Question.create!(
        title: 'ก. การสอบสวนดังกล่าวจะต้องดำเนินการโดยผู้ที่เกี่ยวข้องกับอุบัติการณ์ที่เกิดขึ้น และต้องสอบสวนโดยเร็ว',
        category: 'sme',
        is_sample: true,
        sequence: 43,
        parent: sub2_title
      )
      Question.create!(
        title: 'ข. มีการนำมาตรการปฏิบัติการแก้ไขที่ได้จากการสอบสวนไปดำเนินการ เพื่อขจัดสาเหตุไม่ให้เกิดซ้ำ',
        category: 'sme',
        is_sample: true,
        sequence: 44,
        parent: sub2_title
      )
      Question.create!(
        title: 'ค. นายจ้างต้องมีการจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
        category: 'sme',
        is_sample: true,
        sequence: 45,
        parent: sub2_title
      )
end
## questions sme
Question.transaction do 
title = TitleQuestion.create!(title: '1. นโยบายด้านความปลอดภัยฯ')
  Question.create!(
    title: '1.1 นายจ้างต้องกำหนดนโยบายความปลอดภัยฯ โดยจัดทำเป็นเอกสารพร้อมทั้งลงนาม',
    category: 'sme',
    data: %w[Platinum Gold Silver Basic Start-up],
    is_sample: false,
    sequence: 1,
    parent: title
  )
  Question.create!(
    title: '1.2 นายจ้างต้องให้ลูกจ้างมีส่วนร่วมในการกำหนดนโยบายด้านความปลอดภัยฯ',
    category: 'sme',
    data: %w[Platinum Gold Silver Basic Start-up],
    is_sample: false,
    sequence: 2,
    parent: title
  )
  Question.create!(
    title: '1.3 มีการทบทวนนโยบายด้านความปลอดภัยฯ ตามระยะเวลาที่เหมาะสม เพื่อให้มั่นใจว่านโยบายที่กำหนดขึ้นมีความเหมาะสมกับสถานประกอบกิจการ',
    category: 'sme',
    data: %w[Platinum Gold Silver Basic Start-up],
    is_sample: false,
    sequence: 3,
    parent: title
  )
  Question.create!(
    title: '1.4 มีการสื่อสารให้ลูกจ้างทราบอย่างทั่วถึง',
    category: 'sme',
    data: %w[Platinum Gold Silver Basic Start-up],
    is_sample: false,
    sequence: 4,
    parent: title
  )
title = TitleQuestion.create!(title: '2. โครงสร้างการบริหารด้านความปลอดภัยฯ')
  Question.create!(
    title: '2.1 นายจ้างต้องกำหนดโครงสร้างการบริหารด้านความปลอดภัยฯ พร้อมทั้งกำหนดบทบาท อำนาจหน้าที่และความรับผิดชอบของหน่วยงาน คณะบุคคล และบุคลากรด้านความปลอดภัยฯ เป็นเอกสารและมีการสื่อสารให้ลูกจ้างทราบอย่างทั่วถึง',
    category: 'sme',
    data: %w[Platinum Gold Silver Basic Start-up],
    is_sample: false,
    sequence: 5,
    parent: title
  )
  Question.create!(
    title: '2.2 นายจ้างต้องแต่งตั้งลูกจ้างระดับบริหารของสถานประกอบกิจการ เป็นผู้แทนฝ่ายบริหารด้านความปลอดภัยฯ',
    category: 'sme',
    data: %w[Platinum Gold Silver],
    is_sample: false,
    sequence: 6,
    parent: title
  )
title = TitleQuestion.create!(title: '3. แผนงานด้านความปลอดภัยฯ และการนำไปปฏิบัติ')
  sub_title = TitleQuestion.create!(
    title: '3.1 การชี้บ่งอันตรายและการประเมินความเสี่ยง',
    parent: title
  )
    Question.create!(
      title: '(1) นายจ้างต้องจัดทำขั้นตอนการดำเนินงานการชี้บ่งอันตรายและการประเมินระดับความเสี่ยง',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 7,
      parent: sub_title
    )
title = TitleQuestion.create!(title: '3. แผนงานด้านความปลอดภัยฯ และการนำไปปฏิบัติ')
  sub_title = TitleQuestion.create!(
    title: '3.1 การชี้บ่งอันตรายและการประเมินความเสี่ยง',
    parent: title
  )
    sub2_title = TitleQuestion.create!(
      title: '(2) นายจ้างต้องชี้บ่งอันตรายและประเมินระดับความเสี่ยง',
      parent: sub_title
    )
      Question.create!(
        title: 'ก. กิจกรรมที่ทำเป็นประจำและไม่เป็นประจำ',
        category: 'sme',
        data: %w[Platinum Gold Silver Basic],
        is_sample: false,
        sequence: 8,
        parent: sub2_title
      )
      Question.create!(
        title: 'ข. พฤติกรรมที่ไม่ปลอดภัยของลูกจ้าง ขีดความสามารถของร่างกาย และท่าทางในการทำงานที่ไม่เหมาะสม',
        category: 'sme',
        data: %w[Platinum Gold Silver Basic],
        is_sample: false,
        sequence: 9,
        parent: sub2_title
      )
      Question.create!(
        title: 'ค. การออกแบบพื้นที่ทำงาน กระบวนการ การติดตั้งเครื่องจักรอุปกรณ์ในการทำงาน สถานีงาน และ
การยศาสตร์',
        category: 'sme',
        data: %w[Platinum],
        is_sample: false,
        sequence: 10,
        parent: sub2_title
      )
      Question.create!(
        title: 'ง. กิจกรรมและสภาพแวดล้อมในการทำงานของผู้รับเหมา บุคคลภายนอก และผู้เยี่ยมชม',
        category: 'sme',
        data: %w[Platinum Gold Silver Basic],
        is_sample: false,
        sequence: 11,
        parent: sub2_title
      )
      Question.create!(
        title: 'จ. การเปลี่ยนแปลงหรือข้อเสนอให้มีการเปลี่ยนแปลงวัสดุอุปกรณ์ กระบวนการ วิธีปฏิบัติงาน หรือกิจกรรมต่าง ๆ ในสถานประกอบกิจการ',
        category: 'sme',
        data: %w[Platinum Gold Silver Basic],
        is_sample: false,
        sequence: 12,
        parent: sub2_title
      )
      Question.create!(
        title: 'ฉ. การปรับปรุงระบบการจัดการด้านความปลอดภัยฯ รวมถึงการเปลี่ยนแปลงชั่วคราวและมีผลต่อการปฏิบัติงาน กระบวนการ และกิจกรรมต่าง ๆ',
        category: 'sme',
        data: %w[Platinum Gold Silver Basic],
        is_sample: false,
        sequence: 13,
        parent: sub2_title
      )
      Question.create!(
        title: 'ช. เมื่อเกิดอุบัติเหตุหรือเหตุการณ์เกือบเกิดอุบัติเหตุ และมีการสอบสวนแล้ว',
        category: 'sme',
        data: %w[Platinum Gold Silver Basic],
        is_sample: false,
        sequence: 14,
        parent: sub2_title
      )
title = TitleQuestion.create!(title: '3. แผนงานด้านความปลอดภัยฯ และการนำไปปฏิบัติ')
  sub_title = TitleQuestion.create!(
    title: '3.1 การชี้บ่งอันตรายและการประเมินความเสี่ยง',
    parent: title
  )
    Question.create!(
      title: '(3) นายจ้างต้องทบทวนการชี้บ่งอันตรายและประเมินระดับความเสี่ยงตามช่วงเวลาที่กำหนด',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 15,
      parent: sub_title
    )
    Question.create!(
      title: '(4) นายจ้างต้องให้ผู้รับเหมา มีส่วนร่วมในการชี้บ่งอันตรายและประเมินระดับความเสี่ยง พร้อมทั้งมีการสื่อสารความเสี่ยงไปยังลูกจ้าง ผู้มีส่วนได้เสีย และผู้ที่เกี่ยวข้อง',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 16,
      parent: sub_title
    )
    Question.create!(
      title: '(5) นายจ้างต้องจัดทำและเก็บบันทึกการชี้บ่งอันตรายและการประเมินความเสี่ยง',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 17,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '3.2 กฎหมายความปลอดภัยฯ',
    parent: title
  )
    Question.create!(
      title: '(1) นายจ้างต้องจัดทำขั้นตอนการดำเนินงาน ในการชี้บ่งและติดตามกฎหมายความปลอดภัยฯ ที่เกี่ยวข้องกับสถานประกอบกิจการ ให้เป็นปัจจุบันอยู่เสมอ',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic Start-up],
      is_sample: false,
      sequence: 18,
      parent: sub_title
    )
    Question.create!(
      title: '(2) นายจ้างต้องปฏิบัติตามข้อกำหนดของกฎหมาย และกำหนดผู้รับผิดชอบในการนำกฎหมายไปปฏิบัติและรักษาไว้ในสถานประกอบกิจการ',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic Start-up],
      is_sample: false,
      sequence: 19,
      parent: sub_title
    )
    Question.create!(
      title: '(3) นายจ้างมีการประเมินความสอดคล้องในการปฏิบัติตามกฎหมายด้านความปลอดภัยฯ',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic Start-up],
      is_sample: false,
      sequence: 20,
      parent: sub_title
    )
    Question.create!(
      title: '(4) นายจ้างต้องจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic Start-up],
      is_sample: false,
      sequence: 21,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '3.3 วัตถุประสงค์และแผนงาน',
    parent: title
  )
    Question.create!(
      title: '(1) นายจ้างต้องจัดทำวัตถุประสงค์ด้านความปลอดภัยฯ โดยจัดทำเป็นเอกสาร',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 22,
      parent: sub_title
    )
title = TitleQuestion.create!(title: '3. แผนงานด้านความปลอดภัยฯ และการนำไปปฏิบัติ')
  sub_title = TitleQuestion.create!(
    title: '3.3 วัตถุประสงค์และแผนงาน',
    parent: title
  )
    sub2_title = TitleQuestion.create!(
      title: '(2) นายจ้างต้องจัดทำแผนงานเป็นเอกสารเพื่อให้บรรลุวัตถุประสงค์ของสถานประกอบกิจการ',
      parent: sub_title
    )
      Question.create!(
        title: 'ก. การกำหนดความรับผิดชอบและอำนาจหน้าที่ในระดับและหน่วยงานที่เกี่ยวข้อง ภายในสถานประกอบกิจการ',
        category: 'sme',
        data: %w[Platinum Gold Silver Basic Start-up],
        is_sample: false,
        sequence: 23,
        parent: sub2_title
      )
      Question.create!(
        title: 'ข. วิธีการในการกำหนดมาตรการควบคุมความเสี่ยง ควรกำหนดลำดับความสำคัญ',
        category: 'sme',
        data: %w[Platinum],
        is_sample: false,
        sequence: 24,
        parent: sub2_title
      )
      Question.create!(
        title: 'ค. กรอบเวลาดำเนินการ',
        category: 'sme',
        data: %w[Platinum Gold Silver Basic],
        is_sample: false,
        sequence: 25,
        parent: sub2_title
      )
title = TitleQuestion.create!(title: '3. แผนงานด้านความปลอดภัยฯ และการนำไปปฏิบัติ')
  sub_title = TitleQuestion.create!(
    title: '3.3 วัตถุประสงค์และแผนงาน',
    parent: title
  )
    Question.create!(
      title: '(3) ดำเนินการตรวจติดตามและวัดผลการปฏิบัติงานอย่างสม่ำเสมอเพื่อให้บรรลุตามวัตถุประสงค์และแผนงาน พร้อมทั้งปรับเปลี่ยนตามความเหมาะสม',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 26,
      parent: sub_title
    )
    Question.create!(
      title: '(4) นายจ้างต้องจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 27,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '3.4 สมรรถนะและการฝึกอบรม',
    parent: title
  )
    Question.create!(
      title: '(1) นายจ้างต้องกำหนดสมรรถนะของลูกจ้างซึ่งมีความเสี่ยงที่จะเกิดอันตรายจากการปฏิบัติงาน โดยพิจารณาจากการศึกษา การฝึกอบรม ทักษะ และประสบการณ์ที่เหมาะสม',
      category: 'sme',
      data: %w[Platinum Gold],
      is_sample: false,
      sequence: 28,
      parent: sub_title
    )
    Question.create!(
      title: '(2) นายจ้างต้องชี้บ่งความจำเป็นในการฝึกอบรมที่เกี่ยวข้องกับความเสี่ยง และระบบการจัดการด้านความปลอดภัยฯ โดยจัดให้มีการฝึกอบรมให้สอดคล้องกับความจำเป็นในการฝึกอบรมที่กำหนดไว้ ประเมินประสิทธิผลของการฝึกอบรม และทบทวนเป็นระยะ',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 29,
      parent: sub_title
    )
    Question.create!(
      title: '(3) นายจ้างต้องจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 30,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '3.5 การสื่อสาร',
    parent: title
  )
    Question.create!(
      title: '(1) นายจ้างต้องจัดทำขั้นตอนการดำเนินงานในการสื่อสารที่เกี่ยวข้องกับอันตราย และระบบการจัดการด้านความปลอดภัยฯ ทั้งภายในและภายนอกสถานประกอบกิจการ ที่เกี่ยวข้องกับผู้รับเหมา บุคคลภายนอก และผู้เยี่ยมชมในสถานที่ทำงาน',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 31,
      parent: sub_title
    )
    Question.create!(
      title: '(2) การนำความคิดเห็น ข้อเสนอแนะ ข้อร้องเรียนของลูกจ้างที่เกี่ยวข้องกับความปลอดภัยฯ มาพิจารณาและดำเนินการ',
      category: 'sme',
      data: %w[Platinum Gold Silver],
      is_sample: false,
      sequence: 32,
      parent: sub_title
    )
    Question.create!(
      title: '(3) มีการดำเนินการกับข้อร้องเรียนทางด้านความปลอดภัยฯ จากภายนอกสถานประกอบกิจการ',
      category: 'sme',
      data: %w[Platinum Gold],
      is_sample: false,
      sequence: 33,
      parent: sub_title
    )
    Question.create!(
      title: '(4) นายจ้างต้องจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 34,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '3.6 การจัดทำเอกสาร
นายจ้างจะต้องจัดทำขั้นตอนการดำเนินงานในการควบคุมเอกสาร ในระบบการจัดการด้านความปลอดภัยฯ เพื่อให้เอกสารมีความทันสมัย และสามารถใช้ตามวัตถุประสงค์ที่ต้องการ โดยต้องควบคุมดังนี้',
    parent: title
  )
    Question.create!(
      title: '(1) มีการอนุมัติเอกสารก่อนนำไปใช้งาน',
      category: 'sme',
      data: %w[Platinum Gold Silver],
      is_sample: false,
      sequence: 35,
      parent: sub_title
    )
    Question.create!(
      title: '(2) มีการปรับปรุงเอกสารตามความจำเป็น',
      category: 'sme',
      data: %w[Platinum Gold Silver],
      is_sample: false,
      sequence: 36,
      parent: sub_title
    )
    Question.create!(
      title: '(3) กรณีมีการแก้ไขเอกสารจะต้องมีการระบุสถานะของการแก้ไข',
      category: 'sme',
      data: %w[Platinum Gold Silver],
      is_sample: false,
      sequence: 37,
      parent: sub_title
    )
    Question.create!(
      title: '(4) เอกสารจะต้องเขียนไว้อย่างชัดเจนและผู้ใช้เอกสารสามารถเข้าใจได้',
      category: 'sme',
      data: %w[Platinum Gold Silver],
      is_sample: false,
      sequence: 38,
      parent: sub_title
    )
    Question.create!(
      title: '(5) มีการป้องกันการนำเอกสารที่ล้าสมัยแล้วไปใช้งาน',
      category: 'sme',
      data: %w[Platinum Gold Silver],
      is_sample: false,
      sequence: 39,
      parent: sub_title
    )
    Question.create!(
      title: '(6) มีการชี้บ่งการควบคุมเอกสารที่มาจากภายนอกสถานประกอบกิจการ',
      category: 'sme',
      data: %w[Platinum Gold],
      is_sample: false,
      sequence: 40,
      parent: sub_title
    )
    Question.create!(
      title: '(7) มีการจัดเก็บบันทึกทางด้านความปลอดภัยฯ ตามระยะเวลาที่กำหนดไว้',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 41,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '3.7 การควบคุมการปฏิบัติงาน
นายจ้างต้องกำหนดขั้นตอนเพื่อจัดการความเสี่ยงจากการปฏิบัติงาน และกิจกรรมต่าง ๆ ซึ่งได้มีการกำหนดไว้ว่าจะต้องมีการดำเนินการ โดยขั้นตอนเพื่อจัดการความเสี่ยง ต้องประกอบด้วย',
    parent: title
  )
    Question.create!(
      title: '(1) ขั้นตอนการดำเนินงาน วิธีการปฏิบัติงาน สำหรับกิจกรรมที่มีความเสี่ยง',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 42,
      parent: sub_title
    )
    Question.create!(
      title: '(2) การปฏิบัติตามเกณฑ์การควบคุมการปฏิบัติงานที่ได้กำหนดไว้',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 43,
      parent: sub_title
    )
    Question.create!(
      title: '(3) การควบคุมที่เกี่ยวข้องกับการจัดซื้อจัดจ้าง',
      category: 'sme',
      data: %w[Platinum],
      is_sample: false,
      sequence: 44,
      parent: sub_title
    )
    Question.create!(
      title: '(4) การควบคุมเกี่ยวข้องกับผู้รับเหมาและผู้เยี่ยมชมในสถานที่ทำงาน',
      category: 'sme',
      data: %w[Platinum Gold],
      is_sample: false,
      sequence: 45,
      parent: sub_title
    )
    Question.create!(
      title: '(5) การเตือนอันตราย',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 46,
      parent: sub_title
    )
    Question.create!(
      title: '(6) การจัดการความเปลี่ยนแปลง',
      category: 'sme',
      data: %w[Platinum],
      is_sample: false,
      sequence: 47,
      parent: sub_title
    )
    Question.create!(
      title: '(7) นายจ้างต้องจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 48,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '3.8 การเตรียมความพร้อมและการตอบโต้ภาวะฉุกเฉิน',
    parent: title
  )
    Question.create!(
      title: '(1) นายจ้างมีการชี้บ่งภาวะฉุกเฉินที่อาจจะเกิดขึ้น',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 49,
      parent: sub_title
    )
    Question.create!(
      title: '(2) นายจ้างต้องจัดทำขั้นตอนการดำเนินงาน สำหรับการเตรียมความพร้อมและการตอบโต้ภาวะฉุกเฉิน',
      category: 'sme',
      data: %w[Platinum Gold],
      is_sample: false,
      sequence: 50,
      parent: sub_title
    )
    Question.create!(
      title: '(3) ในการวางแผนตอบโต้ภาวะฉุกเฉิน นายจ้างต้องพิจารณาถึงการประสานงานกับหน่วยงานที่เกี่ยวข้อง ทั้งในการขอความช่วยเหลือและการแจ้งเหตุ',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 51,
      parent: sub_title
    )
    Question.create!(
      title: '(4) ทำการฝึกอบรมให้กับลูกจ้างทุกคนในสถานประกอบกิจการ รวมทั้งทำการฝึกซ้อมแผนตอบโต้ภาวะฉุกเฉินตามช่วงเวลาที่กำหนดไว้',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic Start-up],
      is_sample: false,
      sequence: 52,
      parent: sub_title
    )
    Question.create!(
      title: '(5) นายจ้างต้องมีการตรวจสอบอุปกรณ์ที่ใช้ในภาวะฉุกเฉินเป็นระยะ',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 53,
      parent: sub_title
    )
    Question.create!(
      title: '(6) นายจ้างต้องจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
      category: 'sme',
      data: %w[Platinum Gold Silver Basic],
      is_sample: false,
      sequence: 54,
      parent: sub_title
    )
title = TitleQuestion.create!(title: '4. การประเมินผลและการทบทวนการจัดการด้านความปลอดภัยฯ')
  sub_title = TitleQuestion.create!(
    title: '4.1 การประเมินผล',
    parent: title
  )
    sub2_title = TitleQuestion.create!(
      title: '(1) การเฝ้าระวังและการวัดผลการปฏิบัติงาน
นายจ้างต้องจัดทำขั้นตอนการดำเนินงานในการเฝ้าระวังและวัดผลการปฏิบัติงาน อย่างสม่ำเสมอ โดยขั้นตอนการดำเนินงานจะต้องครอบคลุมถึง',
      parent: sub_title
    )
      Question.create!(
        title: 'ก. การตรวจวัดสภาพแวดล้อมในการทำงาน',
        category: 'sme',
        data: %w[Platinum Gold Silver Basic],
        is_sample: false,
        sequence: 55,
        parent: sub2_title
      )
      Question.create!(
        title: 'ข. การตรวจสุขภาพของลูกจ้างตามปัจจัยเสี่ยง',
        category: 'sme',
        data: %w[Platinum Gold Silver Basic],
        is_sample: false,
        sequence: 56,
        parent: sub2_title
      )
      Question.create!(
        title: 'ค. การตรวจสอบความปลอดภัยทั่วไป',
        category: 'sme',
        data: %w[Platinum Gold],
        is_sample: false,
        sequence: 57,
        parent: sub2_title
      )
      Question.create!(
        title: 'ง. การสังเกตการทำงาน',
        category: 'sme',
        data: %w[Platinum Gold],
        is_sample: false,
        sequence: 58,
        parent: sub2_title
      )
      Question.create!(
        title: 'จ. การตรวจสอบเครื่องจักรอุปกรณ์',
        category: 'sme',
        data: %w[Platinum Gold Silver Basic Start-up],
        is_sample: false,
        sequence: 59,
        parent: sub2_title
      )
      Question.create!(
        title: 'ฉ. การบำรุงรักษาอุปกรณ์เชิงป้องกัน',
        category: 'sme',
        data: %w[Platinum],
        is_sample: false,
        sequence: 60,
        parent: sub2_title
      )
      Question.create!(
        title: 'ช. การตรวจสอบอุปกรณ์คุ้มครองความปลอดภัยส่วนบุคคล',
        category: 'sme',
        data: %w[Platinum Gold Silver Basic Start-up],
        is_sample: false,
        sequence: 61,
        parent: sub2_title
      )
    sub2_title = TitleQuestion.create!(
      title: '(2) การสอบสวนอุบัติการณ์
นายจ้างต้องจัดทำขั้นตอนการดำเนินงานสำหรับสอบสวนและวิเคราะห์อุบัติการณ์ โดย',
      parent: sub_title
    )
      Question.create!(
        title: 'ก. การสอบสวนดังกล่าวจะต้องดำเนินการโดยผู้ที่เกี่ยวข้องกับอุบัติการณ์ที่เกิดขึ้น และต้องสอบสวนโดยเร็ว',
        category: 'sme',
        data: %w[Platinum Gold Silver Basic],
        is_sample: false,
        sequence: 62,
        parent: sub2_title
      )
      Question.create!(
        title: 'ข. มีการนำมาตรการปฏิบัติการแก้ไขที่ได้จากการสอบสวนไปดำเนินการ เพื่อขจัดสาเหตุไม่ให้เกิดซ้ำ',
        category: 'sme',
        data: %w[Platinum Gold Silver Basic],
        is_sample: false,
        sequence: 63,
        parent: sub2_title
      )
      Question.create!(
        title: 'ค. ผลของการสอบสวนจะต้องมีการสื่อสารให้ลูกจ้างในสถานประกอบกิจการ ทราบ',
        category: 'sme',
        data: %w[Platinum Gold],
        is_sample: false,
        sequence: 64,
        parent: sub2_title
      )
      Question.create!(
        title: 'ง. นายจ้างต้องมีการจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
        category: 'sme',
        data: %w[Platinum Gold Silver Basic],
        is_sample: false,
        sequence: 65,
        parent: sub2_title
      )
    sub2_title = TitleQuestion.create!(
      title: '(3) การตรวจประเมิน',
      parent: sub_title
    )
      Question.create!(
        title: 'ก. นายจ้างต้องมีการตรวจประเมินระบบการจัดการด้านความปลอดภัยฯ',
        category: 'sme',
        data: %w[Platinum Gold Silver],
        is_sample: false,
        sequence: 66,
        parent: sub2_title
      )
      Question.create!(
        title: 'ข. นายจ้างต้องจัดทำขั้นตอนการดำเนินงานในการตรวจประเมิน เพื่อกำหนดความสามารถของผู้ตรวจประเมิน ขอบเขต ความถี่ วิธีการ และการรายงานผลการตรวจประเมิน',
        category: 'sme',
        data: %w[Platinum Gold],
        is_sample: false,
        sequence: 67,
        parent: sub2_title
      )
      Question.create!(
        title: 'ค. ผู้ตรวจประเมินจะต้องมีความเป็นกลาง โดยเป็นอิสระจากกิจกรรมที่ตรวจประเมิน',
        category: 'sme',
        data: %w[Platinum Gold],
        is_sample: false,
        sequence: 68,
        parent: sub2_title
      )
      Question.create!(
        title: 'ง. นายจ้างต้องจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
        category: 'sme',
        data: %w[Platinum Gold Silver],
        is_sample: false,
        sequence: 69,
        parent: sub2_title
      )
  sub_title = TitleQuestion.create!(
    title: '4.2 การทบทวน',
    parent: title
  )
    sub2_title = TitleQuestion.create!(
      title: '(1) ผู้บริหารระดับสูงของสถานประกอบกิจการ ต้องทบทวนระบบการจัดการด้านความปลอดภัยฯ ตามช่วงเวลาที่กำหนดไว้ ข้อมูลสำหรับการทบทวน การจัดการต้องรวมถึง',
      parent: sub_title
    )
      Question.create!(
        title: 'ก. นโยบายความปลอดภัยฯ',
        category: 'sme',
        data: %w[Platinum Gold Silver],
        is_sample: false,
        sequence: 70,
        parent: sub2_title
      )
      Question.create!(
        title: 'ข. การบรรลุวัตถุประสงค์ด้านความปลอดภัยฯ',
        category: 'sme',
        data: %w[Platinum Gold Silver],
        is_sample: false,
        sequence: 71,
        parent: sub2_title
      )
      Question.create!(
        title: 'ค. ข้อเสนอแนะ ข้อร้องเรียนด้านความปลอดภัยฯ',
        category: 'sme',
        data: %w[Platinum Gold],
        is_sample: false,
        sequence: 72,
        parent: sub2_title
      )
      Question.create!(
        title: 'ง. สถานะการสอบสวนอุบัติการณ์ ความไม่สอดคล้อง การปฏิบัติการแก้ไข และการปฏิบัติการป้องกัน',
        category: 'sme',
        data: %w[Platinum Gold Silver],
        is_sample: false,
        sequence: 73,
        parent: sub2_title
      )
      Question.create!(
        title: 'จ. ผลการตรวจประเมิน',
        category: 'sme',
        data: %w[Platinum Gold],
        is_sample: false,
        sequence: 74,
        parent: sub2_title
      )
      Question.create!(
        title: 'ฉ. การปฏิบัติตามกฎหมายและผลการปฏิบัติ',
        category: 'sme',
        data: %w[Platinum Gold Silver],
        is_sample: false,
        sequence: 75,
        parent: sub2_title
      )
      Question.create!(
        title: 'ช. การติดตามผลการประชุมครั้งที่ผ่านมา',
        category: 'sme',
        data: %w[Platinum Gold Silver],
        is_sample: false,
        sequence: 76,
        parent: sub2_title
      )
      Question.create!(
        title: 'ซ. การเปลี่ยนแปลงทั้งภายในและภายนอก ที่มีผล
กระทบต่อการจัดทำระบบ',
        category: 'sme',
        data: %w[Platinum Gold],
        is_sample: false,
        sequence: 77,
        parent: sub2_title
      )
      Question.create!(
        title: 'ฌ. ข้อเสนอแนะเพื่อการปรับปรุง',
        category: 'sme',
        data: %w[Platinum Gold Silver],
        is_sample: false,
        sequence: 78,
        parent: sub2_title
      )
    Question.create!(
      title: '(2) นายจ้างต้องจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
      category: 'sme',
      data: %w[Platinum Gold Silver],
      is_sample: false,
      sequence: 79,
      parent: sub_title
    )
title = TitleQuestion.create!(title: '5. การดำเนินการปรับปรุงด้านความปลอดภัยฯ')
  sub_title = TitleQuestion.create!(
    title: '5.1 การแก้ไข การปฏิบัติการแก้ไขและการปฏิบัติการป้องกัน
นายจ้างต้องจัดทำขั้นตอนการดำเนินงานสำหรับการแก้ไขปฏิบัติการแก้ไขความไม่สอดคล้องที่เกิดขึ้นและการปฏิบัติการป้องกันแนวโน้มความไม่สอดคล้องที่อาจจะเกิดขึ้น โดยครอบคลุมถึง',
    parent: title
  )
    Question.create!(
      title: '(1) การแก้ไขความไม่สอดคล้องที่พบ',
      category: 'sme',
      data: %w[Platinum Gold Silver],
      is_sample: false,
      sequence: 80,
      parent: sub_title
    )
    Question.create!(
      title: '(2) การชี้บ่งอันตรายและวิเคราะห์สาเหตุของความไม่สอดคล้องกับข้อกำหนด และดำเนินการปฏิบัติการแก้ไข เพื่อหลีกเลี่ยงการเกิดซ้ำ',
      category: 'sme',
      data: %w[Platinum Gold Silver],
      is_sample: false,
      sequence: 81,
      parent: sub_title
    )
    Question.create!(
      title: '(3) การชี้บ่งอันตรายและวิเคราะห์สาเหตุของแนวโน้มที่จะทำให้เกิดความไม่สอดคล้องตามข้อกำหนดและดำเนินการปฏิบัติการป้องกัน เพื่อป้องกันไม่ให้เกิดความไม่สอดคล้องขึ้น',
      category: 'sme',
      data: %w[Platinum Gold Silver],
      is_sample: false,
      sequence: 82,
      parent: sub_title
    )
    Question.create!(
      title: '(4) ทบทวนประสิทธิผลในการปฏิบัติการแก้ไขและการปฏิบัติการป้องกัน',
      category: 'sme',
      data: %w[Platinum Gold Silver],
      is_sample: false,
      sequence: 83,
      parent: sub_title
    )
    Question.create!(
      title: '(5) สื่อสารผลการปฏิบัติการแก้ไขและการปฏิบัติการป้องกัน',
      category: 'sme',
      data: %w[Platinum Gold],
      is_sample: false,
      sequence: 84,
      parent: sub_title
    )
    Question.create!(
      title: '(6) นายจ้างต้องมีการจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
      category: 'sme',
      data: %w[Platinum Gold Silver],
      is_sample: false,
      sequence: 85,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '5.2 การกำหนดระยะเวลาในการปรับปรุงและพัฒนาระบบการจัดการด้านความปลอดภัยฯ',
    parent: title
  )
    Question.create!(
      title: '(1) นายจ้างนำผลที่ได้จากการทบทวนการจัดการ ไปดำเนินการปรับปรุงระบบการจัดการด้านความปลอดภัยฯ อย่างน้อยปีละ 1 ครั้ง',
      category: 'sme',
      data: %w[Platinum Gold Silver],
      is_sample: false,
      sequence: 86,
      parent: sub_title
    )
    Question.create!(
      title: '(2) นายจ้างนำผลการทบทวนการจัดการไปสื่อสารให้กับลูกจ้างที่เกี่ยวข้อง',
      category: 'sme',
      data: %w[Platinum Gold],
      is_sample: false,
      sequence: 87,
      parent: sub_title
    )
    Question.create!(
      title: '(3) นายจ้างต้องมีการจัดทำและเก็บบันทึกที่เกี่ยวข้อง',
      category: 'sme',
      data: %w[Platinum Gold Silver],
      is_sample: false,
      sequence: 88,
      parent: sub_title
    )
end
## sample questions conflagration
Question.transaction do
title = TitleQuestion.create!(title: '1. การป้องกันและระงับอัคคีภัย')
  sub_title = TitleQuestion.create!(
    title: '1.1 แผนป้องกันและระงับอัคคีภัยในสถานประกอบกิจการ',
    parent: title
  )
    Question.create!(
      title: '(1) มีแผนป้องกันและระงับอัคคีภัยในสถานประกอบกิจการ',
      category: 'conflagration',
      is_sample: true,
      sequence: 1,
      parent: sub_title
    )
    Question.create!(
      title: '(2) มีการกำหนดบุคลากรผู้รับผิดชอบตามแผนป้องกันและระงับอัคคีภัย',
      category: 'conflagration',
      is_sample: true,
      sequence: 2,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '1.2 ระบบสัญญาณแจ้งเหตุเพลิงไหม้',
    parent: title
  )
    Question.create!(
      title: '(1) มีอุปกรณ์ตรวจจับเพียงพอและครอบคลุมทั่วทั้งอาคารสถานประกอบกิจการตามความเหมาะสม',
      category: 'conflagration',
      is_sample: true,
      sequence: 3,
      parent: sub_title
    )
    Question.create!(
      title: '(2) มีอุปกรณ์แจ้งเหตุเพลิงไหม้เพียงพอและครอบคลุมทั่วทั้งอาคารสถานประกอบกิจการ',
      category: 'conflagration',
      is_sample: true,
      sequence: 4,
      parent: sub_title
    )
    Question.create!(
      title: '(3) ในพื้นที่ที่ไม่มีคนงานปฏิบัติงานประจำ (เช่น โกดัง) มีการติดตั้งหรือใช้งานอุปกรณ์ไฟฟ้า หรือจัดเก็บวัตถุไวไฟ หรือวัสดุติดไฟได้ง่าย ต้องติดตั้งอุปกรณ์ตรวจจับและแจ้งเหตุเพลิงไหม้แบบอัตโนมัติ',
      category: 'conflagration',
      is_sample: true,
      sequence: 5,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '1.3 เครื่องดับเพลิงแบบมือถือ',
    parent: title
  )
    Question.create!(
      title: '(1) ต้องติดตั้งในทุกพื้นที่ของอาคารสถานประกอบกิจการ ระยะห่างกันไม่เกิน 20 เมตร',
      category: 'conflagration',
      is_sample: true,
      sequence: 6,
      parent: sub_title
    )
    Question.create!(
      title: '(2) มีป้ายหรือสัญลักษณ์มองเห็นได้ชัดเจนและไม่มีสิ่งกีดขวาง',
      category: 'conflagration',
      is_sample: true,
      sequence: 7,
      parent: sub_title
    )
    Question.create!(
      title: '(3) ต้องมีการตรวจสอบสภาพถังดับเพลิงอย่างน้อย 6 เดือนต่อหนึ่งครั้ง',
      category: 'conflagration',
      is_sample: true,
      sequence: 8,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '1.4 การตรวจสอบ ทดสอบและบำรุงรักษาระบบและอุปกรณ์ต่าง ๆ',
    parent: title
  )
    Question.create!(
      title: '(1) มีการบันทึกรายงานการตรวจสอบและทดสอบระบบและอุปกรณ์ป้องกันและระงับอัคคีภัย',
      category: 'conflagration',
      is_sample: true,
      sequence: 9,
      parent: sub_title
    )
title = TitleQuestion.create!(title: '2. ความปลอดภัยทั่วไปเกี่ยวกับอัคคีภัย')
  Question.create!(
    title: '2.1 มีป้ายความปลอดภัยและป้ายสัญลักษณ์เตือนอันตราย',
    category: 'conflagration',
    is_sample: true,
    sequence: 10,
    parent: title
  )
end
## questions conflagration
Question.transaction do
title = TitleQuestion.create!(title: '1. การป้องกันและระงับอัคคีภัย')
  sub_title = TitleQuestion.create!(
    title: '1.1 แผนป้องกันและระงับอัคคีภัยในสถานประกอบกิจการ',
    parent: title
  )
  sub2_title = TitleQuestion.create!(
      title: '(1) มีแผนป้องกันและระงับอัคคีภัยในสถานประกอบกิจการ',
      parent: sub_title
    )
      Question.create!(
        title: 'ก. แผนการตรวจตรา',
        category: 'conflagration',
        is_sample: false,
        sequence: 1,
        parent: sub2_title
      )
      Question.create!(
        title: 'ข. แผนการอบรม',
        category: 'conflagration',
        is_sample: false,
        sequence: 2,
        parent: sub2_title
      )
      Question.create!(
        title: 'ค. แผนรณรงค์ป้องกัน',
        category: 'conflagration',
        is_sample: false,
        sequence: 3,
        parent: sub2_title
      )
      Question.create!(
        title: 'ง. แผนดับเพลิง',
        category: 'conflagration',
        is_sample: false,
        sequence: 4,
        parent: sub2_title
      )
      Question.create!(
        title: 'จ. แผนอพยพหนีไฟ',
        category: 'conflagration',
        is_sample: false,
        sequence: 5,
        parent: sub2_title
      )
      Question.create!(
        title: 'ฉ. แผนบรรเทาทุกข์',
        category: 'conflagration',
        is_sample: false,
        sequence: 6,
        parent: sub2_title
      )
    Question.create!(
      title: '(2) มีการกำหนดบุคลากรผู้รับผิดชอบตามแผนป้องกันและระงับอัคคีภัย',
      category: 'conflagration',
      is_sample: false,
      sequence: 7,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '1.2 ระบบสัญญาณแจ้งเหตุเพลิงไหม้',
    parent: title
  )
    Question.create!(
      title: '(1) มีอุปกรณ์ตรวจจับเพียงพอและครอบคลุมทั่วทั้งอาคารสถานประกอบกิจการตามความเหมาะสม',
      category: 'conflagration',
      is_sample: false,
      sequence: 8,
      parent: sub_title
    )
    Question.create!(
      title: '(2) มีอุปกรณ์แจ้งเหตุเพลิงไหม้เพียงพอและครอบคลุมทั่วทั้งอาคารสถานประกอบกิจการ',
      category: 'conflagration',
      is_sample: false,
      sequence: 9,
      parent: sub_title
    )
    Question.create!(
      title: '(3) ในพื้นที่ที่ไม่มีคนงานปฏิบัติงานประจำ (เช่น โกดัง) มีการติดตั้งหรือใช้งานอุปกรณ์ไฟฟ้า หรือจัดเก็บวัตถุไวไฟ หรือวัสดุติดไฟได้ง่าย ต้องติดตั้งอุปกรณ์ตรวจจับและแจ้งเหตุเพลิงไหม้แบบอัตโนมัติ',
      category: 'conflagration',
      is_sample: false,
      sequence: 10,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '1.3 ระบบน้ำดับเพลิง',
    parent: title
  )
    Question.create!(
      title: '(1) ต้องจัดเตรียมน้ำสำหรับดับเพลิงในปริมาณที่เพียงพอที่จะส่งจ่ายน้ำให้กับอุปกรณ์ฉีดน้ำดับเพลิงได้อย่างต่อเนื่องเป็นเวลาไม่น้อยกว่า 30 นาที',
      category: 'conflagration',
      is_sample: false,
      sequence: 11,
      parent: sub_title
    )
    Question.create!(
      title: '(2) กรณีมีการจัดเก็บสารเคมีไวไฟต้องจัดเตรียมน้ำสำหรับดับเพลิงในปริมาณที่เพียงพอที่จะส่งจ่ายน้ำให้กับอุปกรณ์ฉีดน้ำดับเพลิงได้อย่างต่อเนื่องเป็นเวลาไม่น้อยกว่า 2 ชั่วโมง',
      category: 'conflagration',
      is_sample: false,
      sequence: 12,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '1.4 ทางออกฉุกเฉิน',
    parent: title
  )
    Question.create!(
      title: '(1) อาคารสถานประกอบกิจการต้องมีทางออกในกรณีฉุกเฉินอย่างน้อย 2 แห่ง อยู่ห่างกัน และต้องไม่มีสิ่งกีดขวาง',
      category: 'conflagration',
      is_sample: false,
      sequence: 13,
      parent: sub_title
    )
    sub2_title = TitleQuestion.create!(
      title: '(2) ประตูทางออกฉุกเฉิน',
      parent: sub_title
    )
      Question.create!(
        title: 'ก. ขนาดกว้างไม่น้อยกว่า 90 เซนติเมตร สูงไม่น้อยกว่า 2 เมตร',
        category: 'conflagration',
        is_sample: false,
        sequence: 14,
        parent: sub2_title
      )
      Question.create!(
        title: 'ข. ประตูต้องทนไฟ เป็นแบบผลักออก ไม่ล่ามโซ่ หรือใส่กุญแจ',
        category: 'conflagration',
        is_sample: false,
        sequence: 15,
        parent: sub2_title
      )
      Question.create!(
        title: 'ค. ป้ายทางออกฉุกเฉินต้องมีไฟส่องสว่างมองเห็นได้ชัดเจน',
        category: 'conflagration',
        is_sample: false,
        sequence: 16,
        parent: sub2_title
      )
  sub_title = TitleQuestion.create!(
    title: '1.5 เครื่องดับเพลิงแบบมือถือ',
    parent: title
  )
    Question.create!(
      title: '(1) ต้องติดตั้งในทุกพื้นที่ของอาคารสถานประกอบกิจการ ระยะห่างกันไม่เกิน 20 เมตร',
      category: 'conflagration',
      is_sample: false,
      sequence: 17,
      parent: sub_title
    )
    Question.create!(
      title: '(2) มีป้ายหรือสัญลักษณ์มองเห็นได้ชัดเจนและไม่มีสิ่งกีดขวาง',
      category: 'conflagration',
      is_sample: false,
      sequence: 18,
      parent: sub_title
    )
    Question.create!(
      title: '(3) ต้องมีการตรวจสอบสภาพถังดับเพลิงอย่างน้อย 6 เดือนต่อหนึ่งครั้ง',
      category: 'conflagration',
      is_sample: false,
      sequence: 19,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '1.6 การตรวจสอบ ทดสอบและบำรุงรักษาระบบและอุปกรณ์ต่าง ๆ',
    parent: title
  )
    Question.create!(
      title: '(1) ต้องตรวจสอบ ทดสอบและบำรุงรักษาระบบ และอุปกรณ์ป้องกันและระงับอัคคีภัยให้พร้อมใช้งานได้ตลอดเวลา (อย่างน้อยเดือนละ 1 ครั้ง หรือตามที่ผู้ผลิตกำหนด)',
      category: 'conflagration',
      is_sample: false,
      sequence: 20,
      parent: sub_title
    )
    Question.create!(
      title: '(2) มีการบันทึกรายงานการตรวจสอบและทดสอบระบบและอุปกรณ์ป้องกันและระงับอัคคีภัย',
      category: 'conflagration',
      is_sample: false,
      sequence: 21,
      parent: sub_title
    )
title = TitleQuestion.create!(title: '2. ระบบไฟฟ้า')
  sub_title = TitleQuestion.create!(
    title: '2.1 การตรวจสอบระบบไฟฟ้าและแผนผังวงจรไฟฟ้า',
    parent: title
  )
    Question.create!(
      title: '(1) มีรายงานการตรวจสอบระบบไฟฟ้าและความปลอดภัยประจำปี และมีวิศวกรไฟฟ้ารับรอง',
      category: 'conflagration',
      is_sample: false,
      sequence: 22,
      parent: sub_title
    )
    Question.create!(
      title: '(2) มีแผนผังวงจรไฟฟ้าที่เป็นปัจจุบันและมีวิศวกรไฟฟ้ารับรอง',
      category: 'conflagration',
      is_sample: false,
      sequence: 23,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '2.2 หม้อแปลงไฟฟ้า',
    parent: title
  )
    Question.create!(
      title: '(1) หม้อแปลงที่ติดตั้งบนพื้น ลานหม้อแปลงมีรั้วรอบ รั้วต้องสูงไม่น้อยกว่า 2 เมตร อยู่ในสภาพดี แข็งแรง',
      category: 'conflagration',
      is_sample: false,
      sequence: 24,
      parent: sub_title
    )
    Question.create!(
      title: '(2) หม้อแปลงที่ติดตั้งบนที่สูง โครงสร้างรับน้ำหนัก เช่น เสา หรือ นั่งร้าน มีความแข็งแรง ไม่แตกร้าว หรือเอียง',
      category: 'conflagration',
      is_sample: false,
      sequence: 25,
      parent: sub_title
    )
    Question.create!(
      title: '(3) สารดูดความชื้น (Silica Gel) มีสีน้ำเงินหรือสีส้ม ไม่เปลี่ยนสี',
      category: 'conflagration',
      is_sample: false,
      sequence: 26,
      parent: sub_title
    )
    Question.create!(
      title: '(4) มีการต่อสายดินที่ถูกต้องตามมาตรฐาน สภาพไม่ชำรุด',
      category: 'conflagration',
      is_sample: false,
      sequence: 27,
      parent: sub_title
    )
    Question.create!(
      title: '(5) มีการติดตั้งอุปกรณ์ป้องกันฟ้าผ่า',
      category: 'conflagration',
      is_sample: false,
      sequence: 28,
      parent: sub_title
    )
  sub_title = TitleQuestion.create!(
    title: '2.3 สายไฟฟ้าและอุปกรณ์',
    parent: title
  )
    Question.create!(
      title: '(1) สายไฟฟ้ามีฉนวนหุ้มอยู่ในสภาพเรียบร้อย การต่อสายจุดต่อสายต้องแน่น ไม่หลวมคลอน และมีการหุ้มฉนวนป้องกัน',
      category: 'conflagration',
      is_sample: false,
      sequence: 29,
      parent: sub_title
    )
    Question.create!(
      title: '(2) อุปกรณ์ไฟฟ้าต้องได้มาตรฐาน เหมาะสมกับการใช้งาน อยู่ในสภาพเรียบร้อยไม่ชำรุด เต้ารับและเต้าเสียบต้องแน่น สายไฟฟ้าลากตามพื้นต้องไม่มีรอยต่อ',
      category: 'conflagration',
      is_sample: false,
      sequence: 30,
      parent: sub_title
    )
  Question.create!(
    title: '2.4 การต่อลงดิน เครื่องจักร อุปกรณ์ที่เป็นโลหะมีการต่อลงดินอย่างถูกต้อง',
    category: 'conflagration',
    is_sample: false,
    sequence: 31,
    parent: title
  )
  sub_title = TitleQuestion.create!(
    title: '2.5 ตู้ควบคุมและแผงสวิตซ์',
    parent: title
  )
    Question.create!(
      title: '(1) สภาพห้องสะอาด เป็นระเบียบเรียบร้อย ตู้ควบคุมและแผงสวิตซ์อยู่ในสภาพใช้งานได้ดี ปลอดภัย',
      category: 'conflagration',
      is_sample: false,
      sequence: 32,
      parent: sub_title
    )
    Question.create!(
      title: '(2) มีพื้นที่สำหรับปฏิบัติงานได้อย่างสะดวก ไม่มีสิ่งกีดขวาง',
      category: 'conflagration',
      is_sample: false,
      sequence: 33,
      parent: sub_title
    )
    Question.create!(
      title: '(3) ในบริเวณตู้ควบคุมและแผงสวิตซ์ต้องมีแสงสว่างที่เพียงพอ มีไฟส่องสว่างฉุกเฉินและถังดับเพลิงที่ถูกประเภท',
      category: 'conflagration',
      is_sample: false,
      sequence: 34,
      parent: sub_title
    )
title = TitleQuestion.create!(title: '2. ระบบไฟฟ้า')
  Question.create!(
    title: '2.6 มีมาตรการป้องกันไฟฟ้าลัดวงจร',
    category: 'conflagration',
    is_sample: false,
    sequence: 35,
    parent: title
  )
  Question.create!(
    title: '2.7 มีระบบป้องกันฟ้าผ่า',
    category: 'conflagration',
    is_sample: false,
    sequence: 36,
    parent: title
  )
title = TitleQuestion.create!(title: '3. หม้อน้ำ/หม้อต้มที่ใช้ของเหลวเป็นสื่อนำความร้อน')
  Question.create!(
    title: '3.1 มีการตรวจทดสอบความปลอดภัยประจำปีของหม้อน้ำ/หม้อต้มฯ โดยมีวิศวกรรับรอง',
    category: 'conflagration',
    is_sample: false,
    sequence: 37,
    parent: title
  )
  Question.create!(
    title: '3.2 มีการตรวจวิเคราะห์คุณภาพน้ำมันของหม้อต้มฯ',
    category: 'conflagration',
    is_sample: false,
    sequence: 38,
    parent: title
  )
  Question.create!(
    title: '3.3 มีการตรวจวิเคราะห์คุณภาพน้ำของหม้อน้ำ',
    category: 'conflagration',
    is_sample: false,
    sequence: 39,
    parent: title
  )
  Question.create!(
    title: '3.4 มีการขึ้นทะเบียนผู้ควบคุมประจำหม้อน้ำ/หม้อต้มฯ ที่มีคุณสมบัติถูกต้อง',
    category: 'conflagration',
    is_sample: false,
    sequence: 40,
    parent: title
  )
title = TitleQuestion.create!(title: '4. ความปลอดภัยสารเคมี')
  Question.create!(
    title: '4.1 มีการจำแนกสารเคมีอันตรายก่อนการจัดเก็บ',
    category: 'conflagration',
    is_sample: false,
    sequence: 41,
    parent: title
  )
  Question.create!(
    title: '4.2 พื้นที่ของอาคารสถานประกอบกิจการที่จัดเก็บวัตถุดิบ หรือผลิตภัณฑ์ ซึ่งเป็นวัตถุที่ติดไฟได้ง่ายต้องกั้นแยกจากพื้นที่ส่วนอื่นของอาคารด้วยผนังกันไฟ',
    category: 'conflagration',
    is_sample: false,
    sequence: 42,
    parent: title
  )
  Question.create!(
    title: '4.3 อาคารเก็บรักษาสารเคมีอันตรายที่มีพื้นที่มากกว่า 1,200 ตารางเมตร ต้องมีผนังกันไป ทุก ๆ ระยะห่างไม่เกิน 40 เมตร',
    category: 'conflagration',
    is_sample: false,
    sequence: 43,
    parent: title
  )
  sub_title = TitleQuestion.create!(
    title: '4.4 สถานที่เก็บรักษา',
    parent: title
  )
    Question.create!(
      title: 'ก. พื้นต้องเรียบ ไม่ลื่น ไม่แตกร้าว',
      category: 'conflagration',
      is_sample: false,
      sequence: 44,
      parent: sub_title
    )
    Question.create!(
      title: 'ข. หากมีระยะห่างจากอาคารอื่นน้อยกว่า 10 เมตร ผนังกันไฟด้านนั้นต้องเป็นชนิดกำแพงกันไฟ',
      category: 'conflagration',
      is_sample: false,
      sequence: 45,
      parent: sub_title
    )
title = TitleQuestion.create!(title: '4. ความปลอดภัยสารเคมี')
  Question.create!(
    title: '4.5 การจัดเก็บก๊าซไวไฟในอาคารต้องติดตั้งเครื่องตรวจวัดปริมาณก๊าซ และมีการระบายอากาศ โดยมากกว่า 2 เท่าของปริมาตรห้องต่อ 1 ชั่วโมง',
    category: 'conflagration',
    is_sample: false,
    sequence: 46,
    parent: title
  )
  Question.create!(
    title: '4.6 การจัดเก็บของเหลวไวไฟในอาคารต้องมีการระบายอากาศ โดยมากกว่า 5 เท่าของปริมาตรห้องต่อ 1 ชั่วโมง',
    category: 'conflagration',
    is_sample: false,
    sequence: 47,
    parent: title
  )
  Question.create!(
    title: '4.7 สถานที่แบ่งบรรจุของเหลวไวไฟควรเป็นห้องที่มีการระบายอากาศที่ดี ขณะถ่ายบรรจุของเหลวไวไฟต้องต่อระบบสายดิน และอุปกรณ์ไฟฟ้าต้องใช้ชนิดป้องกันการระเบิด (Explosion Proof) และต้องไม่แบ่งบรรจุในสถานที่จัดเก็บ',
    category: 'conflagration',
    is_sample: false,
    sequence: 48,
    parent: title
  )
  Question.create!(
    title: '4.8 ภาชนะบรรจุสารเคมีอันตรายต้องจัดวางซ้อนกันสูงไม่เกินกว่าผู้ผลิตกำหนด แต่ไม่เกิน 3 เมตร หากจำเป็นต้องวาง ให้วางบนชั้นวาง (Rack)',
    category: 'conflagration',
    is_sample: false,
    sequence: 49,
    parent: title
  )
  sub_title = TitleQuestion.create!(
    title: '4.9 มีบุคลากรเฉพาะในการจัดเก็บสารเคมีอันตรายเมื่อ',
    parent: title
  )
    Question.create!(
      title: 'ก. ผู้ผลิต ผู้นำเข้า หรือผู้ส่งออกวัตถุอันตราย ที่มีวัตถุอันตรายชนิดที่ 1 ชนิดที่ 2 หรือชนิดที่ 3 ปริมาณรวมตั้งแต่ 1,000 เมตริกตัน/ปี ขึ้นไป',
      category: 'conflagration',
      is_sample: false,
      sequence: 50,
      parent: sub_title
    )
    Question.create!(
      title: 'ข. ผู้ครอบครองวัตถุอันตรายที่มีพื้นที่การเก็บตั้งแต่ 300 ตารางเมตรขึ้นไป',
      category: 'conflagration',
      is_sample: false,
      sequence: 51,
      parent: sub_title
    )
    Question.create!(
      title: 'ค. ผู้ผลิต ผู้นำเข้า ผู้ส่งออก หรือผู้ครอบครองวัตถุอันตรายที่เป็นวัตถุไวไฟ หรือวัตถุออกซิไดซ์และวัตถุเปอร์ออกไซด์',
      category: 'conflagration',
      is_sample: false,
      sequence: 52,
      parent: sub_title
    )
title = TitleQuestion.create!(title: '5. ความปลอดภัยทั่วไปเกี่ยวกับอัคคีภัย')
  Question.create!(
    title: '5.1 การปฏิบัติงานที่ทำให้เกิดความร้อนหรือประกายไฟที่ไม่ใช่งานปกติต้องมีระบบขออนุญาต (Hot Work Permit)',
    category: 'conflagration',
    is_sample: false,
    sequence: 53,
    parent: title
  )
  Question.create!(
    title: '5.2 การจัดเก็บวัตถุสิ่งของที่ติดไฟได้ในอาคาร หากเป็นการเก็บกอง กองวัตถุนั้นต้องสูงไม่เกิน 6 เมตร และมีระยะห่างจากโคมไฟไม่น้อยกว่า 60 เซนติเมตร',
    category: 'conflagration',
    is_sample: false,
    sequence: 54,
    parent: title
  )
title = TitleQuestion.create!(title: '5. ความปลอดภัยทั่วไปเกี่ยวกับอัคคีภัย')
  sub_title = TitleQuestion.create!(
    title: '5.3 การป้องกันอัคคีภัยจากแหล่งก่อเกิดการ
กระจายตัวของความร้อน',
    parent: title
  )
    Question.create!(
      title: 'ก. มีมาตรการป้องกันลูกไฟหรือเขม่าไฟกระเด็นถูกวัตถุที่ติดไฟได้ของเครื่องยนต์หรือปล่องไฟ',
      category: 'conflagration',
      is_sample: false,
      sequence: 55,
      parent: sub_title
    )
    Question.create!(
      title: 'ข. มีมาตรการป้องกันการแผ่รังสี การนำหรือการพาความร้อนจากแหล่งกำเนิดความร้อนสูงไปสู่วัสดุที่ติดไฟได้ง่าย',
      category: 'conflagration',
      is_sample: false,
      sequence: 56,
      parent: sub_title
    )
    Question.create!(
      title: 'ค. มีมาตรการป้องกันเครื่องจักรหรือเครื่องมือที่เกิดประกายไฟหรือความร้อนสูง จากการเสียดสีหรือเสียดทานที่อาจทำให้เกิดการลุกไหม้ได้',
      category: 'conflagration',
      is_sample: false,
      sequence: 57,
      parent: sub_title
    )
    Question.create!(
      title: 'ง. มีมาตรการควบคุมการเชื่อมหรือตัดโลหะที่เป็นแหล่งความร้อนสูงที่อาจทำให้เกิดการลุกไหม้',
      category: 'conflagration',
      is_sample: false,
      sequence: 58,
      parent: sub_title
    )
    Question.create!(
      title: 'จ. มีมาตรการป้องกันไฟฟ้าสถิตจากการเสียดสีของวัสดุ',
      category: 'conflagration',
      is_sample: false,
      sequence: 59,
      parent: sub_title
    )
  Question.create!(
    title: '5.4 มีป้ายความปลอดภัยและป้ายสัญลักษณ์เตือนอันตราย',
    category: 'conflagration',
    is_sample: false,
    sequence: 60,
    parent: title
  )
end

## members
Member.create!(
  title: 'Ms' ,
  firstname: 'Tony',
  lastname: 'Stark',
  phone: '0987654321',
  citizen_id: '1234567890007',
  email: 'ironman@avenger.com',
  password: '123456',
  # remote_avatar_url: 'http://icons.iconarchive.com/icons/mattahan/ultrabuuf/512/Comics-Ironman-Flying-icon.png'
)

Member.create!(
  title: 'Mr',
  firstname: 'Steve',
  lastname: 'Rogers',
  phone: '0987654322',
  citizen_id: '1234567890015',
  email: 'captain_america@avenger.com',
  password: '123456',
  # remote_avatar_url: 'http://icons.iconarchive.com/icons/mattahan/ultrabuuf/512/Comics-Captain-America-Shield-icon.png'
)

Member.create!(
  title: 'Mr',
  firstname: 'Bruce',
  lastname: 'Banner',
  phone: '0987654323',
  citizen_id: '1234567890023',
  email: 'hulk@avenger.com',
  password: '123456',
  # remote_avatar_url: 'http://icons.iconarchive.com/icons/mattahan/ultrabuuf/512/Comics-Hulk-Fist-icon.png'
)

## devices
Device.create!(
  member: Member.last,
  token: '123456'
)

## histories
sme_questions = Question.category('sme').sample?(false)

def sme_questions_level(level)
  Question.category('sme').sample?(false).level(level)
end

History.create!(
  member: Member.last,
  data: {
    category: 'sme',
    level: 'Platinum',
    answers: Question.as_json(sme_questions_level('Platinum')),
    lacks: Question.as_json(sme_questions - sme_questions_level('Platinum'))
  }
)

History.create!(
  member: Member.last,
  data: {
    category: 'sme',
    level: 'Gold',
    answers: Question.as_json(sme_questions_level('Gold')),
    lacks: Question.as_json(sme_questions - sme_questions_level('Gold'))
  }
)

History.create!(
  member: Member.last,
  data: {
    category: 'sme',
    level: 'Silver',
    answers: Question.as_json(sme_questions_level('Silver')),
    lacks: Question.as_json(sme_questions - sme_questions_level('Silver'))
  }
)

History.create!(
  member: Member.last,
  data: {
    category: 'sme',
    level: 'Basic',
    answers: Question.as_json(sme_questions_level('Basic')),
    lacks: Question.as_json(sme_questions - sme_questions_level('Basic'))
  }
)

History.create!(
  member: Member.last,
  data: {
    category: 'sme',
    level: 'Start-up',
    answers: Question.as_json(sme_questions_level('Start-up')),
    lacks: Question.as_json(sme_questions - sme_questions_level('Start-up'))
  }
)

History.create!(
  member: Member.last,
  data: {
    category: 'sme',
    level: 'no',
    answers: [],
    lacks: Question.as_json(sme_questions)
  }
)

conflagration_questions = Question.category('conflagration')
answers = conflagration_questions.sample(10)

History.create!(
  member: Member.last,
  data: {
    category: 'conflagration',
    level: "#{(answers.count * 100.0 / conflagration_questions.count).round(2)}%",
    answers: Question.as_json(answers),
    lacks: Question.as_json(conflagration_questions - answers)
  }
)

ebook01 = 'คู่มือฝึกอบรมการประเมินและการจัดการความเสี่ยงในสถานที่ทำงาน สำหรับสถานประกอบกิจการขนาดกลางและขนาดเล็ก'
infographic21 = 'หลักการจัดเก็บสารเคมี/วัตถุอันตรายในอาคาร'

## media
%w[ebook infographic standard video].map do |folder|
  filenames = Dir.entries(Rails.root.join('app', 'media', folder)).sort.drop(2)

  filenames.map do |filename|
    path = Rails.root.join('app', 'media', folder, filename)
    if filename.split('.').last != 'docx'
      title = filename.gsub('.png', '').gsub('.jpg', '')
      sequence = filename.first(2)
      docx = Rails.root.join('app', 'media', folder, "#{sequence}.docx")
      url =  Docx::Document.open(docx).paragraphs.map(&:to_s).first

      title = title[2..-1] if folder == 'ebook' || folder == 'standard' || folder == 'video'
      title = title[3..-1] if folder == 'infographic'

      title = ebook01 if folder == 'ebook' && sequence.to_i == 1
      title = infographic21 if folder == 'infographic' && sequence.to_i == 21

      Medium.create!(
        title: title,
        category: folder,
        sequence: sequence.to_i,
        url: url,
        image: File.open(path)
      )
    end
  end
end
