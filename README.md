# README

รายการติดตั้ง

* Ruby version 2.6.0

* Rails version 5.2.3

* Postgres version 11.4 

* ImageMagick

ขั้นตอนการติดตั้ง

    $ gem install bundler
    $ bundle install
    $ rails db:create 
    $ rails db:migrate 
    $ rails db:seed

ขั้นตอนการเปิดระบบ

    $ rails server

ขั้นตอนการเข้าใช้ระบบควบคุม

    $ rails console

ขั้นตอนการเข้าใช้ระบบควบคุม ผ่าน website

    {YOUR_HOST}/admin
    
- username: iam
- password: jonsnow