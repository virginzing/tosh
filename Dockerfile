FROM ruby:2.6.0
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
RUN mkdir /tosh
WORKDIR /tosh
COPY Gemfile /tosh/Gemfile
COPY Gemfile.lock /tosh/Gemfile.lock
RUN bundle install
COPY . /tosh

EXPOSE 3000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]