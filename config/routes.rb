Rails.application.routes.draw do
  delete  'admin', to: 'session#logout'

  namespace :admin do
    resources :media_categories
    resources :media
    resources :question_categories
    resources :title_questions
    resources :questions
    resources :members
    resources :histories

    root to: 'questions#index'
  end

  constraints format: :json do
    post  '/login',                         to: 'members#sign_in'
    get   '/logout',                        to: 'members#sign_out'
    post  '/users',                         to: 'members#sign_up'
    get   '/forgot_password',               to: 'members#forgot_password'
    get   '/users/:user_id',                to: 'members#profile'
    put   '/users/:user_id',                to: 'members#update_profile'
    put   '/users/:user_id/password',       to: 'members#change_password'

    post  '/photos',                        to: 'images#upload'

    get   '/media',                         to: 'media#index'

    get   '/questions',                     to: 'questions#index'

    post  '/users/:user_id/answers',        to: 'histories#create'
    get   '/users/:user_id/answers',        to: 'histories#index'
    get   '/users/:user_id/answers/:id',    to: 'histories#show'
  end

  get 'file/:type/*path',                   to: 'files#show'
end
