class CitizenIdValidate
  class << self
    def new(number)
      valid?(number)
    end

    private

    def valid?(number)
      return false if number.nil?

      return unless number.size == 13

      sum = 0

      number.chars.each_with_index.map  do |n, i|
        x = 13 - i
        return valid_checksum?(sum, n) if x == 1

        sum += n.to_i * x
      end
    end

    def valid_checksum?(sum, number)
      mod = sum % 11

      (mod > 1) ? mod + number.to_i == 11 : mod + number.to_i == 1
    end
  end
end
