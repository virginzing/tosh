class MemberMailer < ApplicationMailer
  def forgot_password(member)
    @member = member
    @url  = 'www.tosh.or.th'

    mail(to: @member.email, subject: '[Tosh App] forgot password')
  end
end
