require "administrate/base_dashboard"

class MemberDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    tosh_id: Field::String,
    title: Field::Select.with_options(
      collection: %w[นาย นาง นางสาว]
    ),
    firstname: Field::String,
    lastname: Field::String,
    phone: Field::String,
    citizen_id: Field::String,
    email: Field::String,
    password: Field::String,
    avatar: Field::Carrierwave,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :tosh_id,
    :citizen_id,
    :title,
    :firstname,
    :lastname,
    :phone,
    :email,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :tosh_id,
    :title,
    :firstname,
    :lastname,
    :phone,
    :citizen_id,
    :email,
    :password,
    :avatar,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :title,
    :firstname,
    :lastname,
    :phone,
    :citizen_id,
    :email,
    :password,
    :avatar,
  ].freeze

  # Overwrite this method to customize how members are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(member)
  #   "Member ##{member.id}"
  # end
end
