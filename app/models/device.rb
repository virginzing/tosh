class Device < ApplicationRecord
  belongs_to :member

  def as_json(*)
    {
      access_token: token,
      user_id: member_id
    }
  end
end
