class Member < ApplicationRecord
  default_scope { order(:created_at) }

  has_many :devices
  has_many :histories

  validate :validate_citizen_id
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates_presence_of :email
  validates_uniqueness_of :email
  validates :password, length: { minimum: 6 }

  after_create :update_tosh_id

  mount_uploader :avatar, ImageUploader

  def as_json(options = nil)
    {
      id: id,
      tosh_id: tosh_id,
      title: title,
      first_name: firstname,
      last_name: lastname,
      phone_number: phone,
      id_card_number: citizen_id,
      email: email,
      photo_url: avatar_url(options)
    }
  end

  private

  def validate_citizen_id
    errors.add(:citizen_id, "invalid citizen id") unless CitizenIdValidate.new(citizen_id)
  end

  def avatar_url(options)
    avatar.url || photo_url(options)
  end

  def photo_url(options)
    return if options.nil?

    options[:photo_url]
  end

  def update_tosh_id
    zeros = '0' * (6 - id.to_s.size)
    year = (Date.today.year + 543).to_s.last(2)

    update!(tosh_id: "tosh#{year}#{zeros}#{id}")
  end
end
