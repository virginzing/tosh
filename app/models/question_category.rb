class QuestionCategory < ApplicationRecord
  default_scope { order(:created_at) }
end