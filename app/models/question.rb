class Question < ApplicationRecord
  belongs_to :parent, class_name: 'TitleQuestion', foreign_key: 'parent_id', optional: true

  default_scope { order(:category, :is_sample, :sequence) }
  scope :category, -> (category) { where(category: category) }
  scope :sample?, -> (is_sample) { where(is_sample: is_sample) }

  scope :level, -> (level) { select { |question| question.data.include?(level) } }

  def as_json(*)
    {
      id: id,
      title: title,
      order: sequence,
      data: data,
      parent_id: parent_id
    }
  end

  def self.as_json(questions)
    list = questions.group_by { |question| question['parent_id'] }

    list.map do |parent_id, question_list|
      {
        titles: titles(TitleQuestion.find(parent_id)),
        questions: question_list.as_json
      }
    end
  end

  def self.titles(parent, list = [])
    return list.reverse if parent.nil?

    list << parent.title

    titles(parent.parent, list)
  end
end
