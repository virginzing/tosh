class TitleQuestion < ApplicationRecord
  default_scope { order(:created_at) }

  belongs_to :parent, class_name: 'TitleQuestion', foreign_key: 'parent_id', optional: true

  def as_json(*)
    {
      id: id,
      title: title,
      parent: parent.as_json
    }
  end
end
