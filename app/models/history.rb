class History < ApplicationRecord
  default_scope { order(:created_at) }

  belongs_to :member, optional: true

  def as_json(*)
    {
      id: id,
      type: category,
      rank: level,
      lacks: lack,
      answers: answers,
      created_at: created_at.to_i
    }
  end

  def self.as_json(histories)
    histories.map do |history|
      {
        id: history.id,
        type: history.category,
        rank: history.level,
        created_at: history.created_at.to_i
      }
    end
  end

  def self.answer(params, member)
    category = params[:type]
    ids = params[:ids]
    answers = answer_questions(category, member, ids)
    lacks = answer_lack(category, member, ids)

    data = {
      category: category,
      level: answer_level(category, answers, lacks, member).gsub('.0%', '%'),
      answers: Question.as_json(answers),
      lacks: Question.as_json(lacks)
    }.as_json

    create(data: data, member: member)
  end

  def category
    data['category']
  end

  def level
    data['level'].gsub('.0%', '%')
  end

  def answers
    data['answers']
  end

  private

  def self.questions(category, member)
    Question.category(category).sample?(member.nil?)
  end

  def self.answer_questions(category, member, ids)
    questions(category, member).where(id: ids)
  end

  def self.answer_lack(category, member, ids)
    questions(category, member).where.not(id: ids)
  end

  def self.answer_level(category, answers, lacks, member)
    return "#{(answers.count * 100.0 / questions(category, member).count).round(2)}%" if category == 'conflagration' || member.nil?

    data = lacks.flat_map { |_| _['data'] }.uniq
    levels = %w[Platinum Gold Silver Basic Start-up]

    levels.each do |level|
      return level if data.exclude?(level)
    end

    'no'
  end

  def lack
    lacks = data['lacks'].flat_map{ |_| _['questions'] }
    if data['category'] == 'conflagration' || member.nil? || level.last == '%'
      return [{
        rank: '100%',
        count: lacks.count,
        questions: Question.as_json(lacks)
      }]
    end

    level = data['level']

    case level
    when 'Platinum' || 'no' || '' || nil
      []
    when 'Gold'
      platinum = lacks.filter { |_| _['data'].include?('Platinum') }

      [
        {
          rank: 'Platinum',
          count: platinum.count,
          questions: Question.as_json(platinum)
        }
      ]
    when 'Silver'
      gold = lacks.filter { |_| _['data'].include?('Gold') }
      platinum = lacks.filter { |_| _['data'].include?('Platinum') }

      [
        {
          rank: 'Gold',
          count: gold.count,
          questions: Question.as_json(gold)
        },
        {
          rank: 'Platinum',
          count: platinum.count,
          questions: Question.as_json(platinum)
        }
      ]
    when 'Basic'
      silver = lacks.filter { |_| _['data'].include?('Silver') }
      gold = lacks.filter { |_| _['data'].include?('Gold') }
      platinum = lacks.filter { |_| _['data'].include?('Platinum') }

      [
        {
          rank: 'Silver',
          count: silver.count,
          questions: Question.as_json(silver)
        },
        {
          rank: 'Gold',
          count: gold.count,
          questions: Question.as_json(gold)
        },
        {
          rank: 'Platinum',
          count: platinum.count,
          questions: Question.as_json(platinum)
        }
      ]
    when 'Start-up'
      basic = lacks.filter { |_| _['data'].include?('Basic') }
      silver = lacks.filter { |_| _['data'].include?('Silver') }
      gold = lacks.filter { |_| _['data'].include?('Gold') }
      platinum = lacks.filter { |_| _['data'].include?('Platinum') }

      [
        {
          rank: 'Basic',
          count: basic.count,
          questions: Question.as_json(basic)
        },
        {
          rank: 'Silver',
          count: silver.count,
          questions: Question.as_json(silver)
        },
        {
          rank: 'Gold',
          count: gold.count,
          questions: Question.as_json(gold)
        },
        {
          rank: 'Platinum',
          count: platinum.count,
          questions: Question.as_json(platinum)
        }
      ]
    when 'no'
      startup = lacks.filter { |_| _['data'].include?('Start-up') }
      basic = lacks.filter { |_| _['data'].include?('Basic') }
      silver = lacks.filter { |_| _['data'].include?('Silver') }
      gold = lacks.filter { |_| _['data'].include?('Gold') }
      platinum = lacks.filter { |_| _['data'].include?('Platinum') }

      [
        {
          rank: 'Start-up',
          count: startup.count,
          questions: Question.as_json(startup)
        },
        {
          rank: 'Basic',
          count: basic.count,
          questions: Question.as_json(basic)
        },
        {
          rank: 'Silver',
          count: silver.count,
          questions: Question.as_json(silver)
        },
        {
          rank: 'Gold',
          count: gold.count,
          questions: Question.as_json(gold)
        },
        {
          rank: 'Platinum',
          count: platinum.count,
          questions: Question.as_json(platinum)
        }
      ]
    end
  end
end
