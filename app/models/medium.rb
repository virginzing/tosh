class Medium < ApplicationRecord
  default_scope { order(:category, :sequence, :created_at) }

  mount_uploader :image, ImageUploader

  before_save :parse_meta

  scope :by_category, -> (category) { where(category: category) }

  def as_json(*)
    {
      type: category,
      id: id,
      title: title,
      url: url,
      image_url: image.url,
      meta: meta
    }
  end
  
  def parse_meta
    return unless meta.is_a? String
  
    self.meta = JSON.parse(meta.gsub('=>', ':'))
  end
end
