class ImagesController < ApplicationController
  def upload
    url = Cloudinary::Uploader.upload(params[:photo])['url']

    if url.present?
      render json: { url: url }, status: :ok
    else
      render json: { message: 'not uploaded' }, status: :bad_request
    end
  end
end
