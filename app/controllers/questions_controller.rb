class QuestionsController < ApplicationController
  def index
    @questions = Question.category(params[:type]).sample?(current_member.nil?)

    render json: Question.as_json(@questions), status: :ok
  end
end
