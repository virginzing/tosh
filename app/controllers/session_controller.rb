class SessionController < ActionController::Base
  def logout
    request.env['HTTP_AUTHORIZATION'] = nil
    http_authenticate
  end

  def http_authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == 'iam' && password == 'jonsnow'
    end
  end
end
