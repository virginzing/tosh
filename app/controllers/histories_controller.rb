class HistoriesController < ApplicationController
  def index
    @member = current_member
    @histories = @member.histories

    render json: History.as_json(@histories).select { |_| _[:type] == params[:type] }, status: :ok
  end

  def show
    @history = History.find_by(id: params[:id])

    if @history.present?
      render json: @history.as_json, status: :ok
    else
      render json: nil, status: :not_found
    end
  end

  def create
    @history = History.answer(answer_params, current_member)

    if @history.save
      render json: @history.as_json, status: :created
    else
      render json: { message: { @history.errors.first[0] => [@history.errors.first[1]] }.to_s }, status: :bad_request
    end
  end

  def answer_params
    params.permit(:type, :ids).merge!(ids: params[:ids].split(','))
  end
end
