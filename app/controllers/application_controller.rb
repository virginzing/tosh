class ApplicationController < ActionController::Base
  skip_before_action :verify_authenticity_token

  def authorization_header
    request.headers['Authorization']
  end

  def authorization_token
    authorization_header.split('Bearer ').last
  end

  def current_member
    return unless authorization_header.present? && authorization_token.present?

    device = Device.find_by(token: authorization_token)

    return unless device.present?

    member = device.member

    member if params[:user_id].nil? || member.id == params[:user_id].to_i
  end
end
