class FilesController < ApplicationController
  def show
    path = params[:path]
    file = File.open(Rails.root.join(path + ".#{params[:type]}"))

    send_file(file, filename: File.basename(file))
  end
end
