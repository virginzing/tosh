class MembersController < ApplicationController

  def sign_in
    @member = Member.find_by(
      email: params[:email],
      password: params[:password]
    )

    respond_to do |format|
      if @member.present?
        device = @member.devices.create!(token: SecureRandom.uuid)

        format.json { render json: device.as_json, status: :ok }
      else
        format.json { render json: { message: "Invalid email or password" }, status: :bad_request }
      end
    end
  end

  def sign_out
    device = Device.find_by(token: authorization_token)

    if device.present?
      device.destroy!

      render json: device, status: :ok
    else
      render json: { message: "device not found" }, status: :bad_request
    end
  end

  def sign_up
    return render json: { message: 'email is already taken' }, status: :bad_request if Member.find_by_email(params[:email]).present?

    @member = Member.new(member_params)

    if @member.save
      device = @member.devices.create!(token: SecureRandom.uuid)

      render json: device.as_json, status: :created
    else
      render json: { message: { @member.errors.first[0] => [@member.errors.first[1]] }.to_s }, status: :bad_request
    end
  end

  def forgot_password
    @member = Member.find_by(email: params[:email])

    if @member.present?
      MemberMailer.forgot_password(@member).deliver_now

      render json: @member.as_json, status: :created
    else
      render json: { message: "member not found" }, status: :bad_request
    end
  end

  def profile
    @member = current_member

    if @member.present?
      render json: @member.as_json, status: :created
    else
      render json: { message: "member not found" }, status: :bad_request
    end
  end

  def update_profile
    @member = current_member
    member = Member.find_by_email(params[:email])
    return render json: { message: 'email is already taken' }, status: :bad_request if member.present? && member != @member

    if @member.present? && @member.update(member_params)
      render json: @member.as_json(params), status: :created
    else
      render json: { message: @member.present? ? { @member.errors.first[0] => [@member.errors.first[1]] }.to_s : "member not found" }, status: :bad_request
    end
  end

  def change_password
    @member = current_member

    if @member.present? && @member.password == params[:old_password] && @member.update(password: params[:new_password])
      render json: @member.as_json, status: :created
    else
      render json: { message: @member.present? ? { @member.errors.first[0] => [@member.errors.first[1]] }.to_s : "member not found" }, status: :bad_request
    end
  end

  private

  def member_params
    params.permit(:title, :first_name, :last_name, :phone_number, :id_card_number, :email, :password, :photo_url)

    p = {}
    p.merge!(title: params[:title]) if params[:title].present?
    p.merge!(firstname: params[:first_name]) if params[:first_name].present?
    p.merge!(lastname: params[:last_name]) if params[:last_name].present?
    p.merge!(phone: params[:phone_number]) if params[:phone_number].present?
    p.merge!(citizen_id: params[:id_card_number]) if params[:id_card_number].present?
    p.merge!(email: params[:email]) if params[:email].present?
    p.merge!(password: params[:password]) if params[:password].present?
    p.merge!(remote_avatar_url: params[:photo_url]) if params[:photo_url].present?

    p
  end
end
