class MediaController < ApplicationController
  def index
    @medium = Medium.by_category(params[:type])

    render json: @medium.as_json, status: :ok
  end
end
