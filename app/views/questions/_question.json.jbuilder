json.extract! question, :id, :title, :category, :is_sample, :data, :created_at, :updated_at
json.url question_url(question, format: :json)
