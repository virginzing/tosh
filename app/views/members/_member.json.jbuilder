json.extract! member, :id, :firstname, :lastname, :phone, :citizen_id, :email, :password, :avatar, :created_at, :updated_at
json.url member_url(member, format: :json)
