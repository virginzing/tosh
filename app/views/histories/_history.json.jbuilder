json.extract! history, :id, :member_id, :data, :created_at, :updated_at
json.url history_url(history, format: :json)
