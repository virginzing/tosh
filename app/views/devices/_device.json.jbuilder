json.extract! device, :id, :member_id, :token, :created_at, :updated_at
json.url device_url(device, format: :json)
